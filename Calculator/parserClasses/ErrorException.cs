/*
 * Class Name : ExpEvaluator.cs
 * Author	  : Neelesh Maurya.    
 * Description: This class implemets the ErrorException class.
 * Purpose    : TS2 Design problem submission.
 * Date		  : Sept 26 2004	 
 * */

using System;

namespace ExpEvalTS2
{
	/// <summary>
	/// Summary description for ErrorException.
	/// </summary>
	public class ErrorException:Exception 
	{
		public ErrorException(long MessageId,string Message)
		{
			_message =Message;
			_messageId = MessageId;
		}
		public ErrorException(string Message)
		{
			_message =Message;
		}
		public ErrorException(long MessageId)
		{
			_messageId = MessageId;
		}
		public string _message; 
		public long  _messageId;
	}
}
