﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Calculator
{
	/// <summary>
	/// Interaction logic for AboutWindow.xaml
	/// </summary>
	public partial class AboutWindow : Window
	{
		public AboutWindow()
		{
			this.InitializeComponent();
		}

		private void closeAboutWindow(object sender, System.Windows.DependencyPropertyChangedEventArgs e)
		{
            closeIT();
		}

		private void closeAboutWindow(object sender, System.Windows.Input.MouseEventArgs e)
		{
            closeIT();
		}

        private void closeIT()
        {
            this.Close();
        }
	}
}