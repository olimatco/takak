﻿// <copyright file="CalculatorWindow.xaml.cs" company="University of Toledo">
// Copyright (c) 04-03-2013 All Right Reserved
// </copyright>
// <author>Hussein S. Al-Olimat, hussein.alolimat@msn.com</author>
// <date>04-03-2013</date>
// <summary>Class representing a CalculatorWindow.xaml.cs entity.</summary>

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Diagnostics;
using System.Text.RegularExpressions;
using System.Windows.Media.Animation;

namespace Calculator
{
    /// <summary>
    /// Interaction logic for CalculatorWindow.xaml
    /// </summary>
    public partial class CalculatorWindow : Window
    {
        actionsClass actionsClassObj;
        CalculatorSpeakRecognition speakReco;
        
        public CalculatorWindow()
        {
            InitializeComponent();

            CalculatorWindow ThisWindow = this.Window;

            actionsClassObj = new actionsClass(ThisWindow);
            speakReco = new CalculatorSpeakRecognition(ThisWindow, actionsClassObj);

            initRecognizer(true);
        }
		
        //boolean to keep track if answer was requested
		bool answerWasRequested = false;

        public enum speakBTNState
        {
            start = 0,
            stop = 1,
        }
		
		private enum showHintsEnum
        {
            hide = 0,
            show = 1,
        }
		
		//start with normal mode
		public speakBTNState btnState = speakBTNState.stop;
        private showHintsEnum hints = showHintsEnum.show;

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            //to set the content of the menu item once the window loaded
            if (TaKak.Properties.Settings.Default.silentMode)
            {
                silentModeMenuItem.Header = "Unsilent";
				silentModeMenuItem.Icon = new System.Windows.Controls.Image{ 
           			Source = new BitmapImage(new Uri("/files/speaker.png", UriKind.Relative)) 
       			};
            }

            else
            {
                silentModeMenuItem.Header = "Silent";
				silentModeMenuItem.Icon = new System.Windows.Controls.Image{ 
           			Source = new BitmapImage(new Uri("/files/speaker_mute.png", UriKind.Relative)) 
       			};
            }
        }

        private void startStopSpeechShortcut(object target, ExecutedRoutedEventArgs e)
        {
            startStopSpeech();
        }

        private void modeSelectMenuItemClicked(object sender, System.Windows.RoutedEventArgs e)
        {
            // TODO: Add event handler implementation here.
			startStopSpeech();
        }
		
		private void silentModeMenuItemClicked(object sender, System.Windows.RoutedEventArgs e)
        {
        	// TODO: Add event handler implementation here.
			silentUnSilentTheApp();
        }
		
        private void minBTNClicked(object sender, System.Windows.RoutedEventArgs e)
        {
            // TODO: Add event handler implementation here.
            this.WindowState = WindowState.Minimized;
        }

        private void exitBTNClicked(object sender, System.Windows.RoutedEventArgs e)
        {
            // TODO: Add event handler implementation here.
            Process.GetCurrentProcess().Kill();
        }

        private void allClearBTN_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            // TODO: Add event handler implementation here.
            allClear();
        }

        private void backSpcBTN_Click(object sender, RoutedEventArgs e)
        {
            deleteLastInput();
        }
		
		public void deleteLastInput(){
            if (expTxtBx.Text != "")
            {
                expTxtBx.Text = actionsClassObj.deletelastEntered(expTxtBx.Text);

                //speak it
                actionsClassObj.speakIT("last entry deleted", true, -1);
            }
		}

        private void powerBTN_Click(object sender, RoutedEventArgs e)
        {
            typeInputIntoTextBox("^");
        }

        private void closeBrktBTN_Click(object sender, RoutedEventArgs e)
        {
            typeInputIntoTextBox(")");
        }

        private void openBrktBTN_Click(object sender, RoutedEventArgs e)
        {
            typeInputIntoTextBox("(");
        }

        private void plusMinusBTN_Click(object sender, RoutedEventArgs e)
        {
            signInvert();
        }

        public void signInvert(){
            if (expTxtBx.Text != "")
            {
                expTxtBx.Text = "(" + expTxtBx.Text + ") x -1";

                findFinalResult();
            }
        }

        private void multBTN_Click(object sender, RoutedEventArgs e)
        {
            typeInputIntoTextBox("x");
        }

        private void plusBTN_Click(object sender, RoutedEventArgs e)
        {
            typeInputIntoTextBox("+");
        }

        private void minusBTN_Click(object sender, RoutedEventArgs e)
        {
            typeInputIntoTextBox("-");
        }

        private void equalsBTN_Click(object sender, RoutedEventArgs e)
        {
            findFinalResult();
        }

        public void findFinalResult()
        {
			answerWasRequested = true;
			
            if (!expTxtBx.Text.Equals(""))
            {
                string answer = actionsClassObj.findResult(expTxtBx.Text, false);

                if (answer.Equals("Error"))
                {
                    expTxtBx.Text = "";
					string res = actionsClassObj.getFinalEditedExpression("") + " = Error";
                    //finalProcLabel.Content = res;
					resultTextBox.Text = res;
                }

                else
                {
                    string res = actionsClassObj.getFinalEditedExpression(answer);
                    expTxtBx.Text = answer;
                    //finalProcLabel.Content = res;
					resultTextBox.Text = res;
                }
            }
        }

        /*
        private void pointBTN_Click(object sender, RoutedEventArgs e)
        {
            typeInputIntoTextBox(".");
        }
         * */

        private void num0BTN_Click(object sender, RoutedEventArgs e)
        {
            typeInputIntoTextBox("0");
        }

        private void num1BTN_Click(object sender, RoutedEventArgs e)
        {
            typeInputIntoTextBox("1");
        }

        private void num2BTN_Click(object sender, RoutedEventArgs e)
        {
            typeInputIntoTextBox("2");
        }

        private void num3BTN_Click(object sender, RoutedEventArgs e)
        {
            typeInputIntoTextBox("3");
        }

        private void num4BTN_Click(object sender, RoutedEventArgs e)
        {
            typeInputIntoTextBox("4");
        }

        private void num5BTN_Click(object sender, RoutedEventArgs e)
        {
            typeInputIntoTextBox("5");
        }

        private void num6BTN_Click(object sender, RoutedEventArgs e)
        {
            typeInputIntoTextBox("6");
        }

        private void num7BTN_Click(object sender, RoutedEventArgs e)
        {
            typeInputIntoTextBox("7");
        }

        private void num8BTN_Click(object sender, RoutedEventArgs e)
        {
            typeInputIntoTextBox("8");
        }

        private void num9BTN_Click(object sender, RoutedEventArgs e)
        {
            typeInputIntoTextBox("9");
        }

        public void typeInputIntoTextBox(string input)
        {
            if(Regex.IsMatch(input, "[0-9]") && answerWasRequested == true)
			{
                allClear();
			}

            string confirmedInput = actionsClassObj.checkInput(input, expTxtBx.Text);

            expTxtBx.Text += confirmedInput;
			answerWasRequested = false;

            //speak it
            if (confirmedInput != "")
            {
                actionsClassObj.speakIT(input, true, -1);
            }

            else
            {
                actionsClassObj.speakIT("input not allowed", true, -1);
            }
        }

        private void speakModeBTNClicked(object sender, System.Windows.RoutedEventArgs e)
        {
           startStopSpeech();
        }

        private void stopSpeakBTNClicked(object sender, System.Windows.RoutedEventArgs e)
        {
            startStopSpeech();
        }

        private void windowMoveEvent(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            // TODO: Add event handler implementation here.
            DragMove();
        }

        public void initRecognizer(bool start)
        {
            if (start)
            {
                //Activate speak mode and start listining
                speakReco.startRecognizer();

                //btnState = speakBTNState.stop;
                startStopSpeech();
            }

            else
            {
                //btnState = speakBTNState.start;
                startStopSpeech();
            }
            
        }

        public void startStopSpeech()
		{
			switch (btnState)
            {
                case speakBTNState.start:
                    btnState = speakBTNState.stop;
					Storyboard speakActivateSryBrd = TryFindResource("speakModeBTNClickedStoryBoard") as Storyboard;
            		if (speakActivateSryBrd != null)
            		{
                		speakActivateSryBrd.Begin();
            		}

            		//change menu item info
					modeMenuItem.Header = "Stop Listening [ F1 ]";
					//modeMenuItem.ToolTip = "Stop Listening (F1)";

                    //check if the recognizer is ready then speak about it
                    if (actionsClassObj.getSpeechRecognizerState())
                    {
                        actionsClassObj.speakIT("I am listening", true, -1);
                    }

                    break;

                case speakBTNState.stop:
					
                    btnState = speakBTNState.start;
            		
                    Storyboard stopSpeakModeStryBrd = TryFindResource("stopSpeakModeStryBrd") as Storyboard;
            		
                    if (stopSpeakModeStryBrd != null)
            		{
                		stopSpeakModeStryBrd.Begin();
            		}

		           //change menu item info
					modeMenuItem.Header = "Start Listening [ F1 ]";

                    actionsClassObj.speakHintsIfHintsModeIsActivated();

                    break;
            }			
		}

		private void aboutMenuItemClicked(object sender, System.Windows.RoutedEventArgs e)
		{
			// TODO: Add event handler implementation here.
			AboutWindow a = new AboutWindow();
			a.Show();
		}

		private void showHintsBtnClicked(object sender, System.Windows.RoutedEventArgs e)
		{
            showHelp();
		}

        public void showHelp()
        {
            switch (hints)
            {
                case showHintsEnum.hide:
                    hints = showHintsEnum.show;
                    Storyboard hideHints = TryFindResource("hideHintsStryBrd") as Storyboard;
                    if (hideHints != null)
                    {
                        hideHints.Begin();
                    }
                    break;

                case showHintsEnum.show:
                    hints = showHintsEnum.hide;
                    Storyboard showHints = TryFindResource("showHintsStryBrd") as Storyboard;
                    if (showHints != null)
                    {
                        showHints.Begin();
                    }

                    actionsClassObj.sayHelpInstructions();

                    break;
            }
        }

        private void textChanged(object sender, TextChangedEventArgs e)
        {
            expTxtBx.ScrollToEnd();
        }

        public void allClear()
        {
            if (expTxtBx.Text != "")
            {
                //calcWindow.finalProcLabel.Content = "";
                resultTextBox.Text = "";
                expTxtBx.Text = "";

                //speak it
                actionsClassObj.speakIT("all clear", true, -1);
            }
        }

        public void silentUnSilentTheApp()
        {
            bool currentState = TaKak.Properties.Settings.Default.silentMode;

            if (currentState)
            {
                TaKak.Properties.Settings.Default.silentMode = false;
                TaKak.Properties.Settings.Default.Save();

                silentModeMenuItem.Header = "Silent";
                actionsClassObj.speakIT("Sound Activated", true, -1);

                silentModeMenuItem.Icon = new System.Windows.Controls.Image
                {
                    Source = new BitmapImage(new Uri("/files/speaker_mute.png", UriKind.Relative))
                };
            }

            else
            {
                actionsClassObj.speakIT("Sound deactivated", true, -1);

                TaKak.Properties.Settings.Default.silentMode = true;
                TaKak.Properties.Settings.Default.Save();

                silentModeMenuItem.Header = "Unsilent";

                silentModeMenuItem.Icon = new System.Windows.Controls.Image
                {
                    Source = new BitmapImage(new Uri("/files/speaker.png", UriKind.Relative))
                };
            }
        }
    }
}