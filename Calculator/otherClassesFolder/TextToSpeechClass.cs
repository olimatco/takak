﻿// <copyright file="TextToSpeechClass.cs" company="University of Toledo">
// Copyright (c) 04-03-2013 All Right Reserved
// </copyright>
// <author>Hussein S. Al-Olimat, hussein.alolimat@msn.com</author>
// <date>04-03-2013</date>
// <summary>Class representing a TextToSpeechClass.cs entity.</summary>

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Speech.Synthesis;

namespace Calculator
{
    class TextToSpeechClass
    {
        SpeechSynthesizer reader;

        public TextToSpeechClass()
        {
            reader = new SpeechSynthesizer();
        }

        //talkType = 1(feedback) or 0(thing to say when the recognizer is running)
        public void speakIT(string text, bool dispose, int speed)
        {
            if (!TaKak.Properties.Settings.Default.silentMode)
            {
                if (dispose)
                {
                    reader.Dispose();
                    reader = new SpeechSynthesizer();
                    reader.Rate = speed;
                    reader.SpeakAsync(changeInputToSpeak(text));
                }

                else
                {
                    reader.SpeakAsync(text);
                }
            }
        }

        private string changeInputToSpeak(string input)
        {
            if (input == "x")
            {
                return "times";
            }

            else if (input == "^")
            {
                return "to the power of";
            }

            else if (input == "(")
            {
                return "open bracket";
            }

            else if (input == ")")
            {
                return "close bracket";
            }

            else if (input == "-")
            {
                return "minus";
            }

            else if (input == ".")
            {
                return "point";
            }

            else
            {
                return input;
            }
        }
    }
}
