﻿// <copyright file="actionsClass.cs" company="University of Toledo">
// Copyright (c) 04-03-2013 All Right Reserved
// </copyright>
// <author>Hussein S. Al-Olimat, hussein.alolimat@msn.com</author>
// <date>04-03-2013</date>
// <summary>Class representing a actionsClass.cs entity.</summary>

using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Text.RegularExpressions;
using System.Speech.Synthesis;
using System.Speech.Recognition;
using ExpEvalTS2;
using TaKak;

namespace Calculator
{
    public class actionsClass
    {
        CalculatorWindow calcWindow;

        public actionsClass(CalculatorWindow calcWindow)
        {
            this.calcWindow = calcWindow;
        }

        public actionsClass()
        {

        }

        string finalEditedExpression = "";

        //to get check with the recognizer class if the recognizer is working!
        public static bool recognizerIsRead = false;

        //initialize new object of Text to speech class
        TextToSpeechClass txtToSpeechObj = new TextToSpeechClass();

        public string checkInput(string input, string expression)
        {
            if (input.Equals(")"))
            {
                return insertCloseBrkt(expression);
            }

            else if (input.Equals("("))
            {
                return insertOpenBrkt(expression);
            }

            else if (input.Equals("^") || input.Equals("x") || input.Equals("+") || input.Equals("-"))
            {

                return addInputIfAllowed(expression, input);
            }

            /*
            else if (input.Equals("."))
            {
                return insertPoint(expression);
            }
            */

            else if (input.Equals("0"))
            {
                string lastInput = getLastInput(expression);

                if (Regex.IsMatch(lastInput, "[0-9]"))
                {
                    return "0";
                }

                else
                {
                    return "";
                }
            }
            
            else
            {
                if (getLastInput(expression).Equals(")"))
                {
                    return "";
                }

                else
                {
                    return input;
                }
            }
        }

        public string findResult(string expression, bool inverted)
        {
            expression = expression.Replace("x", "*");

            expression = addMissingCloseBrkts(expression);

            object result;
            
            bool errorFound = false;
            
            try
            {
                string resultString = findExpResult(expression);

                double res = Double.Parse(resultString);

                // check if invert button is clicked
                if (inverted)
                {
                    res = res * -1;
                }

                result = res;
            }

            catch (Exception ee)
            {
                result = ee.Message;
                errorFound = true;
            }

            if (errorFound)
            {
                speakIT("Error", true, -1);

                return "Error";
            }

            else
            {
                string finalResult = result.ToString();

                //to read the inserted expression and the final result.. depth aware
                readInsertedExpressionAndFinalResult(finalResult);

                return finalResult;
            }
        }

        public string findExpResult(string expression)
        {
            ExpTreeNode[] sk = new ExpTreeNode[10];

            string _expression = expression;

            try
            {
                IParser par = new ExpParser();
                ExpEvaluator eu = new ExpEvaluator(par);

                eu.SetExpression(_expression);
                double res = eu.Evaluate();

                return res.ToString();
            }

            catch (ErrorException ka)
            {
                return ka._message;
            }
        }

        private void readInsertedExpressionAndFinalResult(string result)
        {
            //true to dispose the synthesizer and start new session
            speakIT("The Answer of the expression", true, -1);

            string sss = readTheStackList();

            speakIT(sss, false, -3);
            speakIT("equals", false, -1);
            speakIT(result, false, -2);
        }

        //----> speak the depth in the expression
        private string readTheStackList()
        {
            string quantOF = " The Quantity of ";
            string closeQant = "  close quantity ";

            string[] nodes = ExpTreeNode.expressionTokens.ToArray();

            bool done = false;

            while (!done)
            {
                if (nodes.Length > 2)
                {
                    int LastIndexOfOperation = 0;

                    for (int i = 0; i < nodes.Length; i++)
                    {
                        if (nodes[i] == "*" || nodes[i] == "^" || nodes[i] == "-" || nodes[i] == "+")
                        {
                            LastIndexOfOperation = i;
                        }
                    }

                    if (nodes[LastIndexOfOperation + 1][0] == ' ')
                    {
                        nodes[LastIndexOfOperation] = nodes[LastIndexOfOperation + 1] + getOperationNameAsString(nodes[LastIndexOfOperation]) + nodes[LastIndexOfOperation + 2];
                    }

                    else
                    {
                        nodes[LastIndexOfOperation] = quantOF + nodes[LastIndexOfOperation + 1] + getOperationNameAsString(nodes[LastIndexOfOperation]) + nodes[LastIndexOfOperation + 2] + closeQant;
                    }

                    //-----------------------------------------------------------
                    nodes[LastIndexOfOperation + 1] = null;
                    nodes[LastIndexOfOperation + 2] = null;

                    List<string> newNodesList = new List<string>();

                    //to remove null values as we already read them
                    for (int i = 0; i < nodes.Length; i++)
                    {
                        if (nodes[i] != null)
                        {
                            newNodesList.Add(nodes[i]);
                        }
                    }

                    nodes = newNodesList.ToArray();
                    //-----------------------------------------------------------
                }

                else
                {
                    done = true;
                }
            }

            MessageBox.Show(nodes[0]);

            ExpTreeNode.expressionTokens.Clear();
            return nodes[0];
        }

        private string getOperationNameAsString(string op)
        {
            if (op.Equals("*"))
            {
                return " times ";
            }

            else if (op.Equals("^"))
            {
                return " to the power of ";
            }

            else if (op.Equals("+"))
            {
                return " plus ";
            }

            else if (op.Equals("-"))
            {
                return " minus ";
            }

            else
            {
                return "Unknown operator";
            }
        }

        private string insertCloseBrkt(string expression)
        {
            string[] bannedPreviousCharacters = { ""," ", ".", "(" };
            string lastInput = getLastInput(expression);

            int pos = Array.IndexOf(bannedPreviousCharacters, lastInput);

            if (pos > -1)
            {
                return "";
            }

            else
            {
                return ")";
            }
        }

        private string insertOpenBrkt(string expression)
        {
            string lastInput = getLastInput(expression);

            if (lastInput.Equals("") || lastInput.Equals(" ") || lastInput.Equals("("))
            {
                return "(";
            }

            else if (lastInput.Equals(")") || Regex.IsMatch(lastInput, "[0-9]"))
            {
                return " x (";
            }

            else if (lastInput.Equals("-"))
            {
                return "1 x (";
            }

            else
            {
                return "";
            }
        }

        private string addInputIfAllowed(string expression, string input)
        {
            string lastInput = getLastInput(expression);

            if (input == "-")
            {
                //if last input is a number or close bracket then the (-) is an operator
                if (Regex.IsMatch(lastInput, "[0-9]") || lastInput == ")")
                {
                    return " " + input + " ";
                }

                else
                {
                    try
                    {
                        //if the first thing to enter in the expression is the minus sign
                        if (expression != "")
                        {
                            lastInput = expression.Substring(expression.Length - 2, 2);

                            if (lastInput == "^ ")
                            {
                                //not allowed minus power
                                return "";
                            }

                            else
                            {
                                return input;
                            }
                        }

                        else
                        {
                            return input;
                        }
                    }

                    catch(Exception ee){

                        string exception = ee.ToString();
                        
                        if (expression == "(")
                        {
                            return input;
                        }

                        else
                        {
                            return "";
                        }
                    }
                }
            }

            else
            {
                string[] bannedPreviousCharacters = { "", " ", "(", "." };

                int pos = Array.IndexOf(bannedPreviousCharacters, lastInput);

                if (pos > -1)
                {
                    return "";
                }

                else
                {
                    return " " + input + " ";
                }
            }
        }

        private string getLastInput(string expression)
        {
            int exprLength = expression.Length;

            if (exprLength > 0)
            {
                string lastInput = expression.Substring(exprLength - 1, 1);
                return lastInput;
            }

            else
            {
                return "";
            }
        }

        public bool checkNumberOfBrkts(string expression)
        {
            int countOpenBrkts = expression.Split('(').Length - 1;
            int countCloseBrkts = expression.Split(')').Length - 1;

            //if the number of open brkts are equal to the close ones then disable the button
            if (countOpenBrkts.Equals(countCloseBrkts))
            {
                return true;
            }

            else
            {
                return false;
            }
        }

        private string addMissingCloseBrkts(string expression)
        {
            string newExpresssion = expression;

            int countOpenBrkts = expression.Split('(').Length - 1;
            int countCloseBrkts = expression.Split(')').Length - 1;

            if (countOpenBrkts != countCloseBrkts)
            {
                int howManyBrkts = countOpenBrkts - countCloseBrkts;

                for (int i = 0; i < howManyBrkts; i++)
                {
                    newExpresssion += ")";
                }
            }

            setFinalEditedExpression(newExpresssion);

            return newExpresssion;
        }

        public void setFinalEditedExpression(string expression)
        {
            try
            {

                //finalEditedExpression = expression.Replace("*", "x");
                finalEditedExpression = expression;

                //add the $$ and the curly brackets after the power sign

                //find all indeces of the power operator
                int[] positions = AllIndexesOf(finalEditedExpression, "^").ToArray();

                int NumberOfCharacterAdded = 0;

                for (int i = 0; i < positions.Length; i++)
                {
                    int currentIndex = positions[i];

                    string newSubString = finalEditedExpression.Substring(currentIndex + NumberOfCharacterAdded);

                    int startIndexOfPowerValue = newSubString.IndexOf("^ ") + 2;

                    //if the power value is between two brackets
                    if (newSubString[startIndexOfPowerValue] == '(')
                    {
                        int lastIndexOfPowerValue = findTheIndexOfTheCloseBracket(startIndexOfPowerValue, newSubString);

                        int length = lastIndexOfPowerValue - startIndexOfPowerValue;

                        string powerValue = newSubString.Substring(startIndexOfPowerValue, length + 1);

                        string firstPart = finalEditedExpression.Substring(0, currentIndex + 2 + NumberOfCharacterAdded);
                        string middlePart = "{" + powerValue + "}";

                        int lengthOfLastPart = firstPart.Length + powerValue.Length;

                        string lastPart = finalEditedExpression.Substring(lengthOfLastPart);
                        finalEditedExpression = firstPart + middlePart + lastPart;
                    }

                    else
                    {
                        int lastIndexOfPowerValue = newSubString.IndexOf(" ", startIndexOfPowerValue);

                        //power value is last token in the expression
                        if (lastIndexOfPowerValue == -1)
                        {
                            lastIndexOfPowerValue = newSubString.Length;
                        }

                        int length = lastIndexOfPowerValue - startIndexOfPowerValue;

                        string powerValue = newSubString.Substring(startIndexOfPowerValue, length);

                        string firstPart = finalEditedExpression.Substring(0, currentIndex + 2 + NumberOfCharacterAdded);
                        string middlePart = "{" + powerValue + "}";

                        int lengthOfLastPart = firstPart.Length + powerValue.Length;

                        string lastPart = finalEditedExpression.Substring(lengthOfLastPart);
                        finalEditedExpression = firstPart + middlePart + lastPart;
                    }

                    NumberOfCharacterAdded += 2;
                }
            }

            catch (Exception ee)
            {
                ee.Message.ToString();
            }
        }

        public List<int> AllIndexesOf(string str, string value)
        {
            List<int> indexes = new List<int>();
            for (int index = 0; ; index = index + value.Length + 1)
            {
                index = str.IndexOf(value, index);
                if (index == -1)
                    return indexes;
                indexes.Add(index);

                //MessageBox.Show(index.ToString());
            }
        }

        private int findTheIndexOfTheCloseBracket(int startIndex, string expression){

            int numberOfOpenBrackets = 0;
            int endIndex = 0;

            for (int i = startIndex; i < expression.Length; i++)
            {
                if (expression[i] == ')')
                {
                    numberOfOpenBrackets--;
                    //MessageBox.Show("- "+ i);
                }

                if (expression[i] == '(')
                {
                    numberOfOpenBrackets++;
                   // MessageBox.Show("+ " + i);
                }

                if (numberOfOpenBrackets == 0)
                {
                    endIndex = i;
                   // MessageBox.Show("break " + i);
                    break;
                }               
            }

            return endIndex;
        }

        public string getFinalEditedExpression(string result)
        {
            finalEditedExpression = "$$ " + finalEditedExpression + " = " + result + " $$";

            return finalEditedExpression;
        }
		
		public string deletelastEntered(string expression)
		{
			int expLength = expression.Length;

            if (expLength > 0)
            {			
				//check will be done here in case the last input is a mathmatical operation
				//(_+_) if its like this the operation will be deleted which is of length 3
				string lastCharacter = expression.Substring(expLength - 1, 1);
				
				if( lastCharacter == " "){
                	expression = expression.Substring(0, expLength - 3);
				}
				
				else{
					expression = expression.Substring(0, expLength - 1);
				}
            }
			
			return expression;
		}

        //to allow the 
        public void setSpeechRecognitionState(bool state)
        {
            recognizerIsRead = state;

            if (state == false)
            {
                speakIT("The speech recognizer is not activated, please check your microphone and restart the application to use the speak mode.", true, -1);
            }
        }

        public bool getSpeechRecognizerState()
        {
            return recognizerIsRead;
        }

        private bool getHintsModeSettingState(){
            return TaKak.Properties.Settings.Default.hintsModeActivated;
        }

        private void setHintsModeSettingsState(bool activateHintsMode){
            TaKak.Properties.Settings.Default.hintsModeActivated = activateHintsMode;
            TaKak.Properties.Settings.Default.Save();
        }

        public void speakIT(string text, bool dispose, int speed){
            txtToSpeechObj.speakIT(text, dispose, speed);
        }

        public void speakHintsIfHintsModeIsActivated()
        {
            //if the hints mode is activate then say to the user all the details to learn/skip for hints mode deactivated
            if (getHintsModeSettingState())
            {
                //check if the recognizer is ready then speak about it
                if (getSpeechRecognizerState())
                {
                    int speed = -1;

                    speakIT("Welcome to Takak, the talking calculator.", true, speed);
                    speakIT("Say Help, to learn more about how to use the application, or press the black circle below", true, speed);
                    speakIT("you can also Say silent, to mute the sound of the application", false, speed);
                    setHintsModeSettingsState(false);
                }
            }
        }

        public void sayHelpInstructions()
        {
            int speed = -1;

            speakIT("to start entering an expression just say start listening and when you are done say stop listening",true, speed);
            speakIT("to find the sum of 23 and 56 say two three plus five six equals", false, speed);
            speakIT("the rest of the operations can be used in the same way, to use the power say power, " +
                            "for the minus say minus and for multiplication say times", false, speed);
            speakIT("to insert brackets say open bracket and close bracket", false, speed);
            speakIT("to change the sign of the expression say, sign invert", false, speed);
            speakIT("to delete last entry say, delete or delete that or backspace", false, speed);
            speakIT("to clear all values in the calculator just say, clear or clear all", false, speed);
            speakIT("also you can say silent and unsilent to mute and unmute the sound of the calculator", false, speed);
            speakIT("thats all you need to know to use Takak", false, speed);
            speakIT("say start listening to start typing into the calculator", false, speed);
        }
    }
}