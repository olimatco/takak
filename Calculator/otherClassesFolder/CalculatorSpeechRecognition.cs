﻿// <copyright file="CalculatorSpeechRecognition.cs" company="University of Toledo">
// Copyright (c) 04-03-2013 All Right Reserved
// </copyright>
// <author>Hussein S. Al-Olimat, hussein.alolimat@msn.com</author>
// <date>04-03-2013</date>
// <summary>Class representing a CalculatorSpeechRecognition.cs entity.</summary>

/*
 * some lines were taken from: http://msdn.microsoft.com/en-us/library/hh361683(v=office.14).aspx
 * 
 * edited by: Hussein Al-Olimat
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Speech.Recognition;
using System.Threading;
using System.Windows;

namespace Calculator
{
    class CalculatorSpeakRecognition
    {

        private SpeechRecognitionEngine recognizer;

        //to visualize the microphone input
        visualizeSound so;

        private CalculatorWindow calcWindow;
        private actionsClass actionsClassObj;

        public CalculatorSpeakRecognition(CalculatorWindow calcWindow, actionsClass actionsClassObj)
        {
            this.calcWindow = calcWindow;
            this.actionsClassObj = actionsClassObj;
            
            // initialize the visualizer
            so = new visualizeSound(calcWindow);
        }

        public void startRecognizer()
        {
            try
            {
                //initialize recognizer and synthesizer
                initializeRecognizer();

                //if input device found then proceed
                if (SelectInputDevice())
                {
                    LoadDictationGrammar();
                }

                recognizer.RecognizeAsync(RecognizeMode.Multiple);

                actionsClassObj.setSpeechRecognitionState(true);
                
                //start virtualizing the progressbar
                so.startVisulization();
            }

            catch (Exception e)
            {
                string errorMsg = e.Message;

                //this will disable the speak mode in the calculator in case something went wrong, like there is no mice
                actionsClassObj.setSpeechRecognitionState(false);

                disableSpeakMode();
            }
        }

        public void initializeRecognizer()
        {
            var selectedRecognizer = (from e in SpeechRecognitionEngine.InstalledRecognizers()
                                      where e.Culture.Equals(Thread.CurrentThread.CurrentCulture)
                                      select e).FirstOrDefault();
            recognizer = new SpeechRecognitionEngine(selectedRecognizer);
        }

        private bool SelectInputDevice()
        {
            bool proceedLoading = true;
            //if OS is above XP
            if (IsOscompatible())
            {
                try
                {
                    recognizer.SetInputToDefaultAudioDevice();
                }
                catch
                {
                    proceedLoading = false; //no audio input device
                }
            }
            //if OS is XP or below 
            else
                ThreadPool.QueueUserWorkItem(InitSpeechRecogniser);
            return proceedLoading;
        }

        private bool IsOscompatible()
        {
            OperatingSystem osInfo = Environment.OSVersion;
            if (osInfo.Version > new Version("6.0"))
                return true;
            else
                return false;
        }

        private void InitSpeechRecogniser(object o)
        {
            recognizer.SetInputToDefaultAudioDevice();
        }

        private void LoadDictationGrammar()
        {
            Choices choice = new Choices();
            choice.Add(new string[] {
                "Open Bracket", "Close Bracket", "zero", "one", "two", "three", "four", "five", "six",
                "seven", "eight", "nine", "plus", "minus", "times",
                "power", "equals", "All Clear", "clear", "delete", "delete that", "backspace", "start listening", "stop listening"
                //, "point","yes", "no"
                , "sign invert", "help", "silent", "unsilent", "show hints"
            });

            // Create a GrammarBuilder object and append the Choices object.
            GrammarBuilder gb = new GrammarBuilder();
            gb.Append(choice);

            // Create the Grammar instance and load it into the speech recognition engine.
            Grammar g = new Grammar(gb);

            recognizer.LoadGrammar(g);

            // Register a handler for the SpeechRecognized event.
            recognizer.SpeechRecognized += new EventHandler<SpeechRecognizedEventArgs>(sre_SpeechRecognized);
        }

        // Create a simple handler for the SpeechRecognized event.
        void sre_SpeechRecognized(object sender, SpeechRecognizedEventArgs e)
        {
			
            /*
            //to get the results recognized with the Recognizer and the confidence of each recognized input
			foreach (RecognizedPhrase phrase in e.Result.Alternates)
  			{
    			MessageBox.Show("    Phrase: " + phrase.Text + "\n    Confidence score: " + phrase.Confidence);
  			}
			*/

            if (calcWindow.btnState == CalculatorWindow.speakBTNState.stop)
            {
                switch (e.Result.Text)
                {
                    case "Open Bracket":
                        calcWindow.typeInputIntoTextBox("(");
                        break;

                    case "Close Bracket":
                        calcWindow.typeInputIntoTextBox(")");
                        break;

                    case "zero":
                        calcWindow.typeInputIntoTextBox("0");
                        break;

                    case "one":
                        calcWindow.typeInputIntoTextBox("1");
                        break;

                    case "two":
                        calcWindow.typeInputIntoTextBox("2");
                        break;

                    case "three":
                        calcWindow.typeInputIntoTextBox("3");
                        break;

                    case "four":
                        calcWindow.typeInputIntoTextBox("4");
                        break;

                    case "five":
                        calcWindow.typeInputIntoTextBox("5");
                        break;

                    case "six":
                        calcWindow.typeInputIntoTextBox("6");
                        break;

                    case "seven":
                        calcWindow.typeInputIntoTextBox("7");
                        break;

                    case "eight":
                        calcWindow.typeInputIntoTextBox("8");
                        break;

                    case "nine":
                        calcWindow.typeInputIntoTextBox("9");
                        break;

                    case "plus":
                        calcWindow.typeInputIntoTextBox("+");
                        break;

                    case "minus":
                        calcWindow.typeInputIntoTextBox("-");
                        break;

                    case "times":
                        calcWindow.typeInputIntoTextBox("x");
                        break;

                    case "power":
                        calcWindow.typeInputIntoTextBox("^");
                        break;

                    case "equals":
                        calcWindow.findFinalResult();
                        break;

                    case "clear":
                        calcWindow.allClear();
                        break;

                    case "All Clear":
                        calcWindow.allClear();
                        break;

                    case "delete":
                        calcWindow.deleteLastInput();
                        break;

                    case "delete that":
                        calcWindow.deleteLastInput();
                        break;

                    case "backspace":
                        calcWindow.deleteLastInput();
                        break;

                    case "stop listening":
                        calcWindow.initRecognizer(false);
                        break;

                    case "sign invert":
                        calcWindow.signInvert();
                        break;
                    case "help":
                        calcWindow.showHelp();
                        break;
                    case "silent":
                        calcWindow.silentUnSilentTheApp();
                        break;
                    case "unsilent":
                        calcWindow.silentUnSilentTheApp();
                        break;
                    default:
                        break;
                }
            }

            else
            {
                //the initial state will only takes the general command not integers
                switch (e.Result.Text)
                {
                    case "start listening":
                        calcWindow.startStopSpeech();
                        break;
                    case "help":
                        calcWindow.showHelp();
                        break;
                    case "silent":
                        calcWindow.silentUnSilentTheApp();
                        break;
                    case "unsilent":
                        calcWindow.silentUnSilentTheApp();
                        break;
                    default:
                        break;
                }
            }
        }

        //this funtion will disable all speak mode function in the application if the microphone is not available or something went wrong with the recoginzer
        private void disableSpeakMode()
        {
            calcWindow.modeMenuItem.IsEnabled = false;
            calcWindow.SpeakModeGrid.IsEnabled = false;
            calcWindow.showHintsBtn.IsEnabled = false;

            calcWindow.modeMenuItem.Opacity = 0.5;
            calcWindow.SpeakModeGrid.Opacity = 0.3;
            calcWindow.showHintsBtn.Opacity = 0.3;
        }
    }
}
