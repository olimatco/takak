﻿/* 
 * code source: http://naudio.codeplex.com/
 * 
 * Edited by: Hussein Al-Olimat @ 02032013
 * 
 */ 

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NAudio.Wave;

namespace Calculator
{
    class visualizeSound
    {
        WaveIn waveIn;
        private CalculatorWindow calcWindow;
        
        public visualizeSound(CalculatorWindow calcWindow)
        {
            this.calcWindow = calcWindow;
        }

        public void startVisulization()
        {
            waveIn = new WaveIn();
            int waveInDevices = WaveIn.DeviceCount;

            for (int waveInDevice = 0; waveInDevice < waveInDevices; waveInDevice++)
            {
                WaveInCapabilities deviceInfo = WaveIn.GetCapabilities(waveInDevice);
            }

            waveIn.DeviceNumber = 0;
            waveIn.DataAvailable += new EventHandler<WaveInEventArgs>(waveIn_DataAvailable);
            int sampleRate = 8000; // 8 kHz
            int channels = 1; // mono
            waveIn.WaveFormat = new WaveFormat(sampleRate, channels);
            waveIn.StartRecording();
        }

        public void stopVisulization()
        {
            if(waveIn != null)
				waveIn.Dispose();
        }

        void waveIn_DataAvailable(object sender, WaveInEventArgs e)
        {
            for (int index = 0; index < e.BytesRecorded; index += 2)
            {
                short sample = (short)((e.Buffer[index + 1] << 8) |
                                        e.Buffer[index + 0]);
                float sample32 = sample / 32768f;

                double voiceValue = sample32;

                calcWindow.progressBar1.Value = voiceValue * 1000;
            }
        }
    }
}
